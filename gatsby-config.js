const basePath = '/rociolengnick'

module.exports = {
  pathPrefix: basePath,
  siteMetadata: {
    title: `ProjectCCR`,
    description: `CCR website.`,
    author: `CCR group`
  },
  plugins: [
    `gatsby-plugin-material-ui`,
    `gatsby-theme-ceteicean`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `src/content/tei`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `src/content/pages`,
        name: `html`,
      },
    },
  ],
}
